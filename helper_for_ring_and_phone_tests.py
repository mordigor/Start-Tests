from start_tests_header import *



def test_receive_call(video_recording: int):
    steps.step_init(4)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert interface.receive_call()
    assert interface.answer_the_call()
    assert test.verify_start_main_screen_reappear(), take_screenshot_on_error("Start home screen "
                                                                              "didn't appear")



def test_ring_delete_shortcut(video_recording: int):
    steps.step_init(7)
    assert test.screen_on(), take_screenshot_on_error(" "": screen_on")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    # assert test.verify_hubs_display(), take_screenshot_on_error("verify_hubs_display failed")
    assert test.verify_edit_mode_after_swipe_to_shortcut_in_messages(), \
        take_screenshot_on_error(" "": "
                                 "verify_edit_mode_after_swipe_to_shortcut_in_messages")
    assert test.press_on_plus_icon_in_mail_place(), \
        take_screenshot_on_error(" "": press on plus icon in mail place")
    assert test.add_shortcut(func_name="via mail"), take_screenshot_on_error("add_shortcut")
    assert not test.remove_shortcut(remove_place="mail") , take_screenshot_on_error(
        " "": Delete Shortcut Not Succeeded")
    assert test.verify_start_main_screen_reappear , take_screenshot_on_error(
        " "": Start main screen not display")


def test_ring_main_functions(video_recording: int):
    steps.step_init(9)
    print("in test_ring_main_functions")
    # test_advanced_settings_disable_show_hints logging.info(inspect.stack()[0][3])

    assert test.screen_on(), take_screenshot_on_error(" "": screen_on")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    # assert test.verify_hubs_display() is True, take_screenshot_on_error(
    #     "verify_hubs_display failed")
    assert test.camera_default_test_ring_add_shortcut_via_plusapp_opened(), take_screenshot_on_error(
        " "": camera_default_app_opened")
    assert test.verify_start_main_screen_reappear(), take_screenshot_on_error(
        " "": Start main screen not display")
    # only with sim to do verification on device if with sim or not
    # assert test.contact_default_app_opened() is True
    # assert test.verify_start_main_screen_reappear() is not None, "Failed Start main screen " \
    #                                                              "not display"

    assert test.messages_default_app_opened(), take_screenshot_on_error(
        " "": messages_default_app_opened")
    assert test.verify_start_main_screen_reappear(), take_screenshot_on_error(
        " "": Start main screen not display")
    assert test.slide_to_favorite_reached_to_device_home_screen(), take_screenshot_on_error(
        " "": slide_to_favorite_reached_to_device_home_screen")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    assert test.slide_to_unlock_hub_reached_to_device_home_screen(), take_screenshot_on_error(
        " "": to reach home screen")


def test_ring_functionality_of_hub(video_recording: int):
    steps.step_init(8)
    assert test.screen_on(), take_screenshot_on_error(" "": screen_on")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": screen_on")
    assert test.camera_default_app_opened(), take_screenshot_on_error(
        " "": camera_default_app_opened")
    assert test.verify_start_main_screen_reappear() , take_screenshot_on_error(
        " "": Back to start main screen")
    assert test.open_gallery(), take_screenshot_on_error("Failed open_gallery")
    assert test.verify_start_main_screen_reappear() , take_screenshot_on_error(
        " "": Start main screen not display")
    assert test.open_camera_by_icon(), take_screenshot_on_error(" "": Open Camera")
    assert test.verify_start_main_screen_reappear() , take_screenshot_on_error(
        " "": Start main screen not display")
    result = test_adb.check_if_phone_has_sim()
    if result:
        steps.step_init(1)
        assert test.by_open_contact_hub_keypad_open(), take_screenshot_on_error(
            " "": open keypad in contact")
        # TODO call and end call --- > In the log add new call
    else:
        steps.step_init(2)
        # logging.info("No Sim On Device: {}", device_folder_name)
        print("No Sim On Device: {}", device_folder_name)
    assert test.open_messages_by_shortcut(), take_screenshot_on_error(
        " "": open_messages_by_shortcut")
    assert test.verify_start_main_screen_reappear(), take_screenshot_on_error(
        "Failed Start main screen not display")


def test_ring_add_shortcut_via_plus(video_recording: int):
    steps.step_init(7)
    assert test.screen_on(), take_screenshot_on_error(" "": screen_on")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    # assert test.verify_hubs_display(), take_screenshot_on_error("verify_hubs_display failed")
    assert test.swipe_to_plus_icon_in_favorite(remove_place="favorite"),\
        take_screenshot_on_error(" "": swipe_to_plus_icon_in_favorite")
    assert test.add_shortcut(func_name="via plus"), take_screenshot_on_error(" "": "
                                                                             "Add Shortcut")
    assert test.verify_start_main_screen_reappear , take_screenshot_on_error(
        " "": verify_start_main_screen_reappear")
    assert test.verify_shortcut_displayed(), take_screenshot_on_error(
        " "": Add Shortcut")
    assert test.remove_shortcut(remove_place="favorite") , take_screenshot_on_error(
        " "": Delete Shortcut Not Succeeded")


def test_recent_apps(video_recording: int):
    steps.step_init(3)
    # logging.info(inspect.stack()[0][3])
    assert test.screen_on(), take_screenshot_on_error(" "": screen_on")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    # assert test.verify_hubs_display(), take_screenshot_on_error("verify_hubs_display failed")
    assert test.verify_at_least_2_plus_icon_display() >= 2, take_screenshot_on_error(
        " "": verify_at_least_2_plus_icon_display")


def test_add_shortcut_via_settings_screen(video_recording: int):
    steps.step_init(11)
    assert test.screen_on(), take_screenshot_on_error(" "": screen on")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": to open start app")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button Not Succeeded")
    assert test.click_on_favorite_apps_and_contacts() == "Select Your Shortcuts", \
        take_screenshot_on_error(" "": click_on_favorite_apps_and_contacts")
    assert test.click_on_my_apps() == "My Apps", take_screenshot_on_error(
        " "": to click on My Apps button")
    assert test.click_on_plus_in_select_shortcut_menu(), \
        take_screenshot_on_error(" "": click_on_plus_in_select_shortcut_menu")
    assert test.add_shortcut(func_name="via setting"), take_screenshot_on_error(" "":"
                                                                                " to add "
                                                                                "shortcut")
    assert test.verify_start_main_screen_reappear() , take_screenshot_on_error(
        " "": Start main screen not display")
    assert test.verify_shortcut_displayed(), take_screenshot_on_error(
        " "": The shortcut not display in favorite apps")
    assert test.remove_shortcut(remove_place="setting"), take_screenshot_on_error(
        " "": to Delete Shortcut")

#
# def test_contact_us(video_recording: int):
#     steps.step_init(4)
#     assert test.open_start(simple=True), take_screenshot_on_error(" "": Login")
#     assert test.swipe_menu() , take_screenshot_on_error(" "": Swipe Menu")
#     assert test.click_on_setting_button() , take_screenshot_on_error(
#         " "": Click On Setting Button")
#     assert test.contact_us(), take_screenshot_on_error(" "": Contact Us")


def test_check_tutorial_displayed(video_recording: int):
    steps.step_init(4)
    assert test.open_start(simple=True), take_screenshot_on_error(" "": Login")
    assert test.swipe_menu(), take_screenshot_on_error(" "" Swipe Menu")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button")
    assert test.check_tutorial_displayed(), take_screenshot_on_error(" "": "
                                                                         "Displayed "
                                                                         "Tutorial")


def test_disable_start(video_recording: int):
    steps.step_init(6)
    assert test.open_start(simple=True), take_screenshot_on_error("open_start")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button")
    assert test.click_on_disable_start().lower() == "Disable Start?".lower(), take_screenshot_on_error(
        " "": click_on_disable_start")
    assert test.disable_possible_periods_appear(), take_screenshot_on_error(
        " "": disable_possible_periods_appear")
    assert  test.verify_device_home_screen_display(), take_screenshot_on_error("didn't return to device homescreen")
    # assert test.by_choose_time_period_start_closed_and_disable(), take_screenshot_on_error(
    #     " "": by_choose_time_period_start_closed_and_disable")


def test_enable_start(video_recording: int):
    steps.step_init(5)
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    assert test.slide_to_unlock_hub_reached_to_device_home_screen(), take_screenshot_on_error(
        " "": slide_to_unlock_hub_reached_to_device_home_screen")
    assert test.set_screen_state(screen_state=0), take_screenshot_on_error(
        " "": set_screen_state off")
    assert test.set_screen_state(screen_state=1), take_screenshot_on_error(
        " "": set_screen_state  on")
    assert test.verify_start_main_screen_reappear(), take_screenshot_on_error(
        " "" Start main screen not display")
    device_obj.press.home()


def test_delete_start_from_device(video_recording: int):
    steps.step_init(7)
    #test.adb.install_start_apk(log=False), take_screenshot_on_error(" "": to install start")
    assert test.open_start(simple=True), take_screenshot_on_error(" "": open_start")
    assert test.slide_to_unlock_hub_reached_to_device_home_screen(), take_screenshot_on_error(
        " "": slide_to_unlock_hub_reached_to_device_home_screen")
    assert test.uninstall_start_app(), take_screenshot_on_error(" "": uninstall start")
    assert test.set_screen_state(screen_state=0), take_screenshot_on_error(
        " "": set_screen_state off")
    assert test.set_screen_state(screen_state=1), take_screenshot_on_error(
        " "": set_screen_state on")
    assert test.verify_device_home_screen_display(unlock_status=1)
    assert test.adb.install_start_apk()


def test_advanced_settings_disable_show_hints(video_recording: int):
    steps.step_init(8)
    assert test.open_start(simple=True), take_screenshot_on_error(" "": to open Start application")
    assert test.swipe_menu(), take_screenshot_on_error(" "": to slide to "
                                                                   "next screen")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button Not Succeeded")
    assert test.click_on_advanced_settings(), take_screenshot_on_error(
        " "": to get to Advanced Settings screen")
    assert test.enable_disable_advanced_settings(text="Show Hints", to_enable=False), \
        take_screenshot_on_error("Show Hints wasn't disabled")
    #environ["RAV_HINTS_DISABLED"] = "TRUE"
    assert test.verify_start_main_screen_reappear() , take_screenshot_on_error(
        " "": to get back to main screen")
    assert test.verify_hints_absent(), take_screenshot_on_error(" "": Hints were not "
                                                                "disabled")
    assert test.enable_disable_advanced_settings_from_home_screen(
        text="Show Hints", to_enable=True), take_screenshot_on_error("********** "": "
                                                                  "Enable Show Hints after test "
                                                                  "finished"
                                                                  "**********")
    #environ["RAV_HINTS_DISABLED"] = "FALSE"


def test_advanced_settings_enable_full_screen(video_recording: int):
    steps.step_init(7)
    assert test.open_start(simple=True), take_screenshot_on_error(" "": to open Start application")
    assert test.swipe_menu(), take_screenshot_on_error(" "": to slide to "
                                                                   "next screen")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button Not Succeeded")
    assert test.click_on_advanced_settings(), take_screenshot_on_error(
        " "": to get to Advanced Settings screen")

    assert test.enable_disable_advanced_settings(text="Full Screen", to_enable=True),\
        take_screenshot_on_error(" "": Unable to enable full")
    assert test.verify_start_main_screen_reappear(), take_screenshot_on_error(
        " "": to get back to main screen")
    assert test.verify_full_screen(), take_screenshot_on_error(" "": to enable Full "
                                                               "Screen")



def test_advanced_settings_disable_show_settings_icon(video_recording: int):
    steps.step_init(6)
    assert test.open_start(simple=True), take_screenshot_on_error(" "": to open Start application")

    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")

    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button Not Succeeded")
    assert test.click_on_advanced_settings(), take_screenshot_on_error(" "": to get to "
                                                                       "Advanced Settings screen")

    assert test.enable_disable_advanced_settings("Show Settings Icon", False), \
        take_screenshot_on_error(" "": Show Settings wasn't disabled")
    assert test.verify_menu_is_absent_on_main_screen(), take_screenshot_on_error(" "": "
                                                                                 "to disable menu")


def test_notifications_missed_events_comparison(video_recording: int):
    steps.step_init(4)
    assert test.open_start(simple=True), take_screenshot_on_error("Failed to open Start application")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button Not Succeeded")
    assert test.click_on_notification_selection(), take_screenshot_on_error(
        " "": to click on Notification Selection")

    # assert test.notification_selection_all(), take_screenshot_on_error(
    # "Didn't receive the Allow this app to access Notifications\' popup")
    # assert test.enable_app_to_access_notifications(), take_screenshot_on_error(
    # "Failed to enable app to access Notifications")
    # assert test.notification_starter_appear_when_screen_off_and_on(), take_screenshot_on_error(
    # "Notification starter doesn't appear on application  main page")
    # assert test.missed_events_comparison(), take_screenshot_on_error("Houston we have a problem")
    #


def test_select_theme_via_settings(video_recording: int):
    steps.step_init(7)
    assert test.open_start(simple=True), take_screenshot_on_error("Failed to open Start application")
    test_adb.screenshot_by_adb(Path(test.device_folder) / "InitialTheme.png")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button() , take_screenshot_on_error(
        " "": Click On Setting Button Not Succeeded")
    assert interface.click_on_Setup_Your_Wallpaper(config.Settings_dict.setup_wallpaper), \
        take_screenshot_on_error(" click on Setup_Your_Wallpaper failed ")
    assert interface.verify_in_Choose_Your_Wallpaper_screen(), take_screenshot_on_error("Not in "
                            "Choose Your Wallpaper screen")
    assert interface.setup_wallpaper() , take_screenshot_on_error("To select theme")
    assert test.verify_theme_changed()


def test_add_more_themes(video_recording: int):
    steps.step_init(6)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    test_adb.screenshot_by_adb(Path(test.device_folder) / "InitialTheme.png")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_plus_for_more_themes(),take_screenshot_on_error("-"": to "
                                                                         "click ")
    assert interface.verify_in_Wallpapers_screen() , take_screenshot_on_error("Not "
                                                      "on Wallppers screen")
    assert test.click_on_new_theme(), take_screenshot_on_error("to return to home "
                                                               "screeen")
    assert test.verify_theme_changed() , take_screenshot_on_error("to change theme")


def test_enable_your_widget(video_recording: int):
    steps.step_init(6)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert test.swipe_menu() , take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button(), take_screenshot_on_error("not in menu screen")
    assert interface.click_on_Setup_Your_Widgets(),take_screenshot_on_error("not in Seup Your "
                                                                            "Widgets screen")
    img1 = test_adb.screenshot_by_adb(Path(test.device_folder) / "ClockScreenShot.png")
    assert img1
    assert interface.click_on_Clock_Widget() , take_screenshot_on_error("didn't click on Clock "
                                                                        "Widget")
    assert test.verify_clock_on_or_off(img1) , take_screenshot_on_error("clock widget wasn't "
                                                                         "enabled/disabled")
    assert interface.bring_clock_widget_to_initial_state()


def test_setup_color_widget(video_recording: int):
    steps.step_init(7)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button(), take_screenshot_on_error("not in menu screen")
    assert interface.click_on_Setup_Your_Widgets(), take_screenshot_on_error("not in Setup Your "
                                                                             "Widgets Screen")
    assert interface.click_on_Widget_Color() , take_screenshot_on_error("not in Color Widget")
    assert interface.set_widget_color_to_black() ,take_screenshot_on_error("failed to set widget "
                                                                           "caller black")
    assert interface.change_widget_color_to_white_and_verify() ,take_screenshot_on_error(
        "Black to white change failed")


def test_verify_issue_with_widget_color(video_recording: int):
    try:
        steps.step_init(3)
        test.open_start(simple=True,logging=False)
        test.swipe_menu(logging=False)
        assert test.click_on_setting_button(),take_screenshot_on_error("got to wrong screen")
        assert interface.click_on_Setup_Your_Widgets(logging=False)
        assert interface.click_on_Widget_Color(), take_screenshot_on_error("not in Color Widget")
        test_adb.go_to_device_home_screen()
        test.open_start(simple=True, logging=False)
        swipe_and_click_on_settings =  interface.swipe_and_click_on_settings()#
        device_obj.press.back()
        device_obj.press.back()
        assert swipe_and_click_on_settings , take_screenshot_on_error("got to wrong screen")
    except Exception as e:
        print (e)

def test_change_widget_size(video_recording: int):
    steps.step_init(8)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert test.swipe_menu(), take_screenshot_on_error(" "": Swipe Menu")
    assert test.click_on_setting_button(), take_screenshot_on_error("not in menu screen")
    assert interface.click_on_Setup_Your_Widgets(), take_screenshot_on_error("not in Setup Your "
                                                                             "Widgets Screen")
    assert  interface.click_on_Widget_Size(), take_screenshot_on_error("click on Widget Size  "
                                                                       "failed")
    assert interface.verify_in_Widget_Size_screen(), take_screenshot_on_error("not in Widget "
                                                                              "Size Screen")
    assert interface.minimize_clock_widget(), take_screenshot_on_error("failed to click on left "
                                                                       "button")
    assert  interface.maximize_widget_size_and_verify() , take_screenshot_on_error("failed to "
                                 "minimize or maximize clock widget")


def test_deny_permission(video_recording: int):
    version = test_adb.get_device_android_version()
    if version >= b"6.0":
        steps.step_init(1)
        assert test.deny_permission(), take_screenshot_on_error(
            " ----> Failed: Displayed And Deny Messages Of Permissions")
    else:
        steps.step_init(0)
        # global print_success
        # print_success = False
        html_log.h_log("Can\'t perform verification on android version lower than 6.0 ")


# def test_messages_of_permissions_displayed_and_allow(video_recording: int):
#     version = test_adb.get_device_android_version()
#     if version >= b"6.0":
#         steps.step_init(1)
#         assert test.messages_of_permissions_displayed()
#     else:
#         steps.step_init(0)
#         # global print_success
#         # print_success = False
#         print("can't perform test because version < 6.0")
#         html_log.h_log("Can\'t perform verification on android version lower than 6.0 ")



def test_stress_tests(video_recording: int):
    steps.step_init(3)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert interface.multiple_clicks_on_ring(100) , take_screenshot_on_error("muktiple clicks on ring caused a failure")
    assert interface.multiple_screen_on_and_off(50), take_screenshot_on_error("multiple screen "
                                                    "on/off caused failure")


def test_stress_tests_2(video_recording: int):
    steps.step_init(4)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert interface.multiple_press_back(100,pref="in_stress_2"), take_screenshot_on_error("multiple press home "
                                                                        "failure")
    assert interface.multiple_click_on_settings_icon(100), take_screenshot_on_error("multiple "
                                   "click on settings failure")
    assert interface.multiple_press_home(100), take_screenshot_on_error("multiple press home "
                                                                        "failure")


def test_back_drawer(video_recording: int):
    steps.step_init(3)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    res = interface.open_upper_drawer()
    print("result = ", res)
    assert res, take_screenshot_on_error("drawer was not opened")
    print("finished open_upper_drawer")
    time.sleep(2)
    img = test_adb.screenshot_by_adb(Path(test.device_folder) / "multiple_press_back_drawer_1.png")
    assert interface.back_from_drawer(img) ,take_screenshot_on_error("not in Srart home page")


def test_back_from_settings_screen(video_recording: int):
    steps.step_init(4)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert interface.swipe_left_by_menu_icon() , take_screenshot_on_error("failed to swipe left by menu")
    res = test.click_on_setting_button()
    assert res, take_screenshot_on_error("failed to get to settings screen")
    assert interface.back_from_settings_screen(), take_screenshot_on_error("not in Srart home page")


def test_back_from_security_screen(video_recording: int):
    steps.step_init(4)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert interface.swipe_left_by_menu_icon(), take_screenshot_on_error(
        "failed to swipe left by menu")
    res = interface.click_on_security()
    assert res, take_screenshot_on_error("not in seacrh screen")
    # assert interface.back_from_security.lock_point_screen(), take_screenshot_on_error("didn't return to "
    #                                                                  "homescreen")
    assert interface.back_from_security_screen(), take_screenshot_on_error("didn't return to homescreen")


def test_drag_clock_widget(video_recording: int):
    steps.step_init(3)
    assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")
    assert interface.verify_default_widget_alignment(),take_screenshot_on_error("verifying that "
         "widget has default alignment failed ")
    assert interface.move_clock_widget(), take_screenshot_on_error("failed to move clock widget")
#
#
# def test_restart(video_recording: int):
#     steps.step_init("oijioj")
#     assert test.open_start(simple=True), take_screenshot_on_error(" to open Start application")

def test_debug():
    # #test.set_black_wallpapper()
    # test.swipe_screen()
    # test.click_on_plus_for_more_themes()
    # interface.click_on_categories()
    # interface.click_on_abstract()
    # time.sleep(8)
    interface.set_black_background_via_more_themes()




