import inspect
import os
from pathlib import Path

import config


class Steps:
    @staticmethod
    def step_init(num):
        os.environ["RAV_TEST_SCOPE_COUNTER"] = str(num)
        os.environ["RAV_NUMBER_OF_STEPS_IN_TEST"] = str(num)

    @staticmethod
    def step_increment():
        tmp = os.environ["RAV_TEST_SCOPE_COUNTER"]
        os.environ["RAV_TEST_SCOPE_COUNTER"] = str(int(tmp)+1)

    @staticmethod
    def step_decrement():
        tmp = os.environ["RAV_TEST_SCOPE_COUNTER"]
        os.environ["RAV_TEST_SCOPE_COUNTER"] = str(int(tmp) - 1)

    @staticmethod
    def get_step_counter():
        return int(os.environ["RAV_TEST_SCOPE_COUNTER"])

steps = Steps()


class HtmlLog:
    def __init__(self):
        self.test_directory_name = str(os.environ["RAVTECH_NOW"]).replace(" ", "_").replace(":",
                                                                                            "_")\
            if "None" not in os.environ["RAVTECH_NOW"] else None

        self.log_file_path = os.environ['test_failures_path'] + "/" + "test_log_" + \
                             self.test_directory_name + ".html"\
            if "None" not in os.environ[
            'test_failures_path'] else None

    # def print_title(self):
    #     print("******** {}".format(self.log_file_path))
    #     self.h_log("*"*80)
    #     self.h_log("Automation Report  for {} test execution".format(os.environ["RAVTECH_NOW"]))

    def h_log(self, message, style=None, link=None,tag_first=None,tag_last=None,new_line=True):
        print("in h_log")
        if new_line:
            nl = "</br>"
        else:
            nl = ""
        if not tag_first:
            tag_first = ""
        if not tag_last:
            tag_last = ""
        # if not style:
        #     style = html_names.test_step
        if link:
            if not style:
                full_message = "{}<a href=\"{}\">{}</a>{}{}".format(tag_first, link, message,
                                                                    tag_last, nl)
            else:
                full_message = "{}<a class=\"{}\" href=\"{}\">{}</a>{}{}".format(tag_first,style,
                                            link, message, tag_last,nl)
        else:
            if not style:
                full_message = "{}<a >{}</a>{}{}".format(tag_first, message, tag_last, nl)
            else:
                full_message = "{}<a class=\"{}\">{}</a>{}{}".format(tag_first,style, message,
                                                                     tag_last,nl)
        print("full_message ", full_message)
        with open(self.log_file_path, 'a+') as f:
            # noinspection PyTypeChecker
            print(full_message, file=f)

    def simple_log(self,message):
        with open(self.log_file_path, 'a+') as f:
            # noinspection PyTypeChecker
            print(message, file=f)

    def convert_message_to_path(self,path):
        print("in convert_message_to_path")
        path = str(path)
        path_array = path.split("/")
        if "home" in path_array:
            path_array.remove("home")
        new_path = "\\".join(path_array)
        full_path = "\\\\" + config.locall_host_ip + new_path
        print("full_path ",full_path)
        return full_path

    def get_step_num(self):
        num_of_steps = os.environ.get("RAV_NUMBER_OF_STEPS_IN_TEST")
        steps_remaining = os.environ.get("RAV_TEST_SCOPE_COUNTER")
        return int(num_of_steps) - int(steps_remaining)

    def decision_logging(self, frm, step_name, caller, behave = False):
        print("in decision logging")
        try:
            if os.environ.get("RAV_BEHAVE") == "TRUE":
                module_name = "start_test"
            else:
                mod = inspect.getmodule(frm[0])
                print(mod)
                module_name = mod.__name__
            print("step_name ", step_name, " caller ", caller)
            if "start_test" in module_name:
                print("_______step name: ", step_name)
                steps.step_decrement()
                step_num = self.get_step_num()
                if  caller not in config.false_steps:
                    print("caller not in config.false_step")
                    if step_num > 1:
                        self.h_log(message="SUCCESS",style=html_names.success_class,
                                   tag_first=html_code.start_new_cell,tag_last=html_code.end_cell,
                                   new_line=False)
                        self.simple_log(html_code.end_row)
                    self.simple_log(html_code.start_new_row)
                    self.h_log(step_name,style=html_names.test_step ,tag_first=html_code.start_new_cell,
                               tag_last=html_code.end_cell,
                               new_line=False)
        except Exception as e:
            print(e)

    def write_log_header_html(self):
        self.simple_log(html_code.header)
        self.simple_log(html_code.body)
        self.simple_log(html_code.body_before)
        self.simple_log(html_code.title_test_device)
        self.simple_log(html_code.title_test)
        self.simple_log(html_code.failure_message)
        self.simple_log(html_code.information)
        self.simple_log(html_code.internal_failure_message)
        self.simple_log(html_code.test_step)
        self.simple_log(html_code.title_test_suite)
        self.simple_log(html_code.table)
        self.simple_log(html_code.cell)
        self.simple_log(html_code.end_style_tag)

    def write_log_bottom_html(self):
        self.simple_log(html_code.bottom)

    def print_two_columns_table(self,arr):
        with open(self.log_file_path, 'a+') as f:
            # noinspection PyTypeChecker
            arr_len = len(arr)
            print('<table>',file=f)
            i = 0
            while i < arr_len:
                if i + 1 <= arr_len - 1:
                    tmp = arr[i + 1]
                else:
                    tmp = ""
                print("<tr>",file=f)
                print("<td>{}</td><td>{}</td>".format(arr[i], tmp),file=f)
                print("</tr>",file=f)
                i = i + 2
            print('</table>',file=f)


def get_devices_selected_but_not_connected(selected, connected):
    if type(selected)==str:
        tmp = selected.split(",")
    else:
        tmp = selected
    devices_selected_but_not_connected = []
    for dev_in_selected in tmp:
        print("divice in selected: ",dev_in_selected)
        dev = dev_in_selected.split(":")
        print("split string by \":\"  ",dev)
        tmp2 = dev[1].strip()
        if tmp2 not in connected:
            devices_selected_but_not_connected.append(dev_in_selected)
    return devices_selected_but_not_connected


def get_selected_and_connected_devices(selected, connected):
    if type(selected)==str:
        tmp = selected.split(",")
    else:
        tmp = selected
    devices_selected_and_connected = []
    for dev_in_selected in tmp:
        print("divice in selected: ",dev_in_selected)
        dev = dev_in_selected.split(":")
        print("split string by \":\"  ",dev)
        tmp2 = dev[1].strip()
        if tmp2 in connected:
            devices_selected_and_connected.append(dev_in_selected)
    return devices_selected_and_connected


def get_scenario_name_if_behave():
    a = None
    try:
        a = os.environ.get("RAV_CURRENT_RUNNING_TEST")
        tmp = a.split("\"")[1]
        tmp2 = tmp.split(" ")
        tmp3 = "_".join(tmp2)
        return tmp3
    except Exception as e:
        print(e)
        if a:
            return a
        else:
            return "------"


def print_paths_on_test_failure():
    html_log = HtmlLog()
    print("running test = ",os.environ.get("RAV_CURRENT_RUNNING_TEST"))
    print("rav behave = ", os.environ.get("RAV_BEHAVE"))
    if os.environ.get("RAV_BEHAVE") == "TRUE":
        print("os.environ.get(RAV_BEHAVE) == TRUE")
        path_to_failure_video = str(Path(os.environ.get('DEVICE_FAILURE_FOLDER')) / "videos" / get_scenario_name_if_behave()) + ".mp4"
    else:
        path_to_failure_video = config.videos_folder / os.environ.get("RAV_CURRENT_RUNNING_TEST")
        path_to_failure_video = html_log.convert_message_to_path(path_to_failure_video)
        path_to_failure_video = path_to_failure_video + ".mp4"

    print("path to failure video = ",path_to_failure_video)
    #todo: change "home " to locall_host_ip
   # path_to_failure_video = "file:///" + str(path_to_failure_video)
    disabled = os.environ.get("DISABLE_TEST_RECORDING")
    video_recording_permited =  "1" #if not int(disabled)  else   "0"
    if video_recording_permited == "1":
        html_log.h_log(message="     failure video: ",
                       link=html_log.convert_message_to_path(path_to_failure_video),
                       style=html_names.failure_message,
                       tag_first=html_code.start_new_cell,
                       tag_last=html_code.end_cell,
                       new_line=False)
    path_to_developers_log = Path(os.environ.get('DEVICE_FAILURE_FOLDER')) / "{}.log".format(
        get_scenario_name_if_behave())
    print("path_to_developers_log = ", path_to_developers_log)
    #path_to_developers_log = "file:///" + str(path_to_developers_log)
    #path_to_developers_log = html_log.convert_message_to_path(path_to_developers_log)
    html_log.h_log(message="     developers log : ",
                   link=html_log.convert_message_to_path(path_to_developers_log),
                   style=html_names.failure_message,
                   tag_first=html_code.start_new_cell,
                   tag_last=html_code.end_cell,
                   new_line=False)


class html_code:
    header = "<!DOCTYPE html><html><body><style>"
    #bgcolor=\"\#ffffff\"
    #html = "html{background-color: blue;}"
    # body = "body{background-color: #98de98;padding: 66px;}"
    body = "body{background - color: transparent; padding: 66 px;}"
    # body = "body{background-image: url(" \
    #        "\"/home/user/Downloads/photo_23951_20130324.jpg\");}"
    body_before = "body:before {background: url("+"file://172.17.252.239/share/workspace/html_images/backgr_1.jpg" + \
                  ");opacity: .3;content: \" \";position: " \
                  "fixed;width: 100%;height: 100vh;top: 0;left: 0;background-size: " \
                  "cover;z-index: -1;}"

    title_test_suite = ".title_test_suite{ color: blue; font-size: 250%; font-weight: bold; }"
    title_test_device = ".title_test_device{ color: black; font-size: 180%; font-weight: normal;font-weight: bold;}"
    title_test = ".title_test{color: black;font-size: 130%;font-weight: normal;font-weight: bold;}"
    failure_message = ".failure_message{ color: red; background-color: turkiz; font-size: " \
                      "100%;font-weight: normal;font-weight: bold;}"
    test_step = ".test_step{color: black;font-size: 100%;font-weight: normal;}"
    information = ".test_step{color: black;font-size: 100%;font-weight: normal;}"
    internal_failure_message = ".internal_failure_message{color: black;background-color: yellow;font-size: 100%;font-weight: normal;font-weight: bold;}"
    #table = "table, th, td {border: 1px solid black;border-collapse: collapse;width: 20%;}"
    table = "table {width:500px; border: 1px solid black;border-collapse: collapse; }"
    row = "tr {width: 500px; border: 1px solid black;border-collapse: collapse;}"
    cell = "td {width:100px; border: 1px solid black;border-collapse: collapse; }"
    success_class = ".success{ color: green; font-size: 110%; font-weight: bold; }"
    end_style_tag = "</style>"
    bottom = "</body></html>"
    start_new_table = "<table>"
    start_new_row = "<tr>"
    start_new_cell = "<td>"
    end_cell = "</td>"
    end_row = "</tr>"
    end_table = "</table>"


class html_names:
    html = "html"
    body = "body"
    header = "header"
    title_test_suite = "title_test_suite"
    title_test_device = "title_test_device"
    title_test = "title_test"
    failure_message = "failure_message"
    test_step = "test_step"
    information = "information"
    internal_failure_message = "internal_failure_message"
    end_style_tag = "end_style_tag"
    bottom = "bottom"
    table = "table"
    start_new_table = "start_new_table"
    start_new_row = "start_new_row"
    start_new_cell = "start_new_cell"
    end_cell = "end_cell"
    end_row = "end_row"
    end_table = "end_table"
    success_class = "success_class"

def dict_to_str_array(dict):
    arr = []
    for k, v in dict.items():
        new = str(k) + ":" + str(v)
        arr.append(new)
    return arr


def print_tests_suit_header(now,devices,devices_dict,test_suit,build_number=None,job_name=None):
    HtmlLog_ = HtmlLog()
    HtmlLog_.write_log_header_html()
    HtmlLog_.h_log("=" * 100)
    HtmlLog_.h_log(message="TEST SUITE NAME:     {}".format(now),
                   style=html_names.title_test_suite)
    if job_name:
        HtmlLog_.h_log(message="Jenkins Job Name: {}".format(job_name),style=html_names.title_test)
    if build_number:
        HtmlLog_.h_log(message="Jenkins Build Number: {}".format(build_number),style=html_names.title_test)
    HtmlLog_.h_log("=" * 100)
    HtmlLog_.h_log("")
    HtmlLog_.h_log("selected tests:", style=html_names.title_test)
    HtmlLog_.print_two_columns_table(test_suit)
    HtmlLog_.h_log("")
    HtmlLog_.h_log("Test will be executed on following devices:",
                   style=html_names.title_test)
    HtmlLog_.print_two_columns_table(get_selected_and_connected_devices(devices, devices_dict))
    # HtmlLog_.h_log("Connected devices:", style=html_names.title_test)
    # HtmlLog_.print_two_columns_table(dict_to_str_array(devices_dict))
    HtmlLog_.h_log("")
    # HtmlLog_.h_log("Selected devices:", style=html_names.title_test)
    # HtmlLog_.print_two_columns_table(devices)
    # HtmlLog_.h_log("")
    # HtmlLog_.h_log("Devices selected but not connected:",
    #                style=html_names.title_test)
    # HtmlLog_.print_two_columns_table(selected_but_not_connected)


if __name__ == '__main__':
    HtmlLog()
