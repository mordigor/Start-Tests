import inspect
from cv2 import cv2

import log_utilities
import os
import settings
from pathlib import Path
from subprocess import check_output, CalledProcessError, Popen
from time import sleep
import config
steps = log_utilities.Steps()
log_html = log_utilities.HtmlLog()
device_id = settings.device_id
device_obj = settings.device_obj


def write_internal_error_to_log():
    if os.environ.get("RAV_INTERNAL_ERROR") == "0":
        os.environ["RAV_INTERNAL_ERROR"] = "1"
        log_html.h_log("     Internal error occured, pleased contact "
                       "Automation team")
        log_utilities.print_paths_on_test_failure()


class AdbFunctions:
    def __init__(self):
        self.device_obj = device_obj
        self.home_path = Path(os.environ.get('HOME_DIRECTORY'))
        print("self device obj in init", self.device_obj)
        self.device = device_id
        self.device_folder = Path(os.environ.get("RAV_DEVICE_FOLDER"))#Path(self.home_path) / device_folder_namekk
        print("self device in init", self.device)

    def get_screen_image_by_name(self,name=None):
        print("in crop_clock_widget")
        img = self.screenshot_by_adb(name_no_device_folder=name)
        img = cv2.imread(img)
        return img

    def crop_clock_widget(self,img=None,name=None):
        print("in crop_clock_widget")
        if img is None:
            img = self.screenshot_by_adb(name_no_device_folder=name)
            img = cv2.imread(img)
        height, width, channels = img.shape
        print("height ", height)
        print("1")
        crop_img = img[int(height * 0.1):int(height * 0.30), 0:width]
        print("2")
        sleep(2)
        cv2.imwrite(str(Path(self.device_folder) / "tmp_clock_cropped.png"),crop_img)
        img = cv2.imread(str(Path(self.device_folder) / "tmp_clock_cropped.png"))
        return img

    def crop_upper_screen(self, img=None, name=None, height_percent = 0.4):
        print("in crop_clock_widget")
        if img is None:
            img = self.screenshot_by_adb(name_no_device_folder=name)
            img = cv2.imread(img)
        height, width, channels = img.shape
        print("height ", height)
        print("1")
        crop_img = img[int(height * 0.1):int(height * height_percent), 0:width]
        print("2")
        sleep(2)
        tmp_img = "tmp_upper_screen_cropped.png"
        cv2.imwrite(str(Path(self.device_folder) / tmp_img),crop_img)
        img = cv2.imread(str(Path(self.device_folder) / tmp_img))
        return img

    def adb(self, command: str, *args):
        print("in adb")
        device_command = "adb -s " + self.device + " " + command
        res = check_output(device_command.format(*args).split())
        print(device_command)
        print("adb result: ",res)
        return res

    def adb_with_wait(self, command: str, *args):
        print("in adb_with_wait")
        device_command = "adb -s " + self.device + " " + command
        process = Popen(device_command.format(*args).split()).wait()
        print("process ", process)
        return process

    def install_start_anyway(self):
        print("in install_start_anyway")
        try:
            apk_path, file_name = self.get_path_for_apk()
            self.adb_with_wait("install -r -d {}", apk_path)
            return True
        except Exception as e:
            print(e)
            return False

    def install_start_apk(self, is_test=True):
        print("in install_start_apk")
        try:
            apk_path, file_name = self.get_path_for_apk()
            if is_test:
                print("1111")
                log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
                process = self.adb_with_wait("install -r -d {}", apk_path)
                if process == 0:
                    return True
            if self.device in config.devices_with_factory_Start:
                print("22222222")
                print("can not install because device has factory installed Start")
                return None
        except Exception as e:
            print(e)
            return False
        try:
            flag = 1
            celltick_package = self.check_if_start_installed()
            if celltick_package:
                print("33333333")
                flag = 2
                version_ = check_output("adb -s {} shell dumpsys package com.celltick.lockscreen | "
                                       "grep versionName".format(self.device).split())
                version = self.parse_adb_result(version_)
                print("start version on device: ", version)
                version_in_server = file_name[0].split("_")[-1]
                version_in_server = Path(version_in_server).stem
                if version_in_server != version:
                    print("444444444444")
                    log_html.h_log("{} APK version will be installed".format(version_in_server),style="title_test")
                    log_html.h_log("")
                    self.adb_with_wait("install -r -d {}", apk_path)
            else:
                self.adb_with_wait("install -r -d {}", apk_path)
                flag = 1
            sleep(10)
            version = check_output("adb -s {} shell dumpsys package com.celltick.lockscreen | "
                                    "grep versionName".format(self.device).split())
            if version:
                print("5555555555")
                return flag
            return False
        except Exception as e:
            print(e)
        return False

    def check_if_start_installed(self):
        celltick_package = self.adb("shell pm list packages com.celltick.lockscreen")
        if celltick_package:
            return True
        return False

    def get_path_for_apk(self):
        print("in get_path_for_apk")
        apk_directory = self.home_path / "apk/"
        if os.environ.get("RAV_NIGHTLY_APK") and "true" in os.environ.get("RAV_NIGHTLY_APK").lower():
            apk_directory = self.home_path / "apk_nightly/"
        file_name = os.listdir(str(apk_directory))
        apk_path = apk_directory / file_name[0]
        print("apk path: ",apk_path)
        return apk_path, file_name

    # def install_start_apk(self, log=True):
    #     print("in install_start_apk")
    #     if self.device in config.devices_with_factory_Start:
    #         print("can not install because device has factory installed Start")
    #         return None
    #     apk_directory = self.home_path / "apk/"
    #     file_name = os.listdir(str(apk_directory))
    #     apk_path = apk_directory / file_name[0]
    #     try:
    #         print("install_start_apk  --1")
    #         result = check_output("adb -s {} shell dumpsys package com.celltick.lockscreen | "
    #                               "grep versionName".format(self.device).split())
    #     except Exception as e:
    #         print("install_start_apk  --1.1")
    #         print(e)
    #         self.adb_with_wait("install -r -d {}", apk_path)
    #         return True
    #
    #     flag = False
    #     try:
    #         print("install_start_apk  --2")
    #         if log:
    #             print("install_start_apk  --2.1")
    #             log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3],
    #                                       inspect.stack()[1][3])
    #             print("apk file name", file_name)
    #             print(apk_path)
    #
    #         celltick_package = self.adb("shell pm list packages com.celltick.lockscreen")
    #         if not celltick_package:
    #             print("install_start_apk  --3")
    #             print("DEBUG start app not installed yet on device {}".format(device_folder_name))
    #             self.adb_with_wait("install -r -d {}", apk_path)
    #             return True
    #         print("versionName result:", result)
    #         if b"versionName" not in result:
    #             print("install_start_apk  --4")
    #             print("start not installed on device")
    #             self.adb_with_wait("install {}", apk_path)
    #             return True
    #         else:
    #             print("install_start_apk  --4.1")
    #             version = self.parse_adb_result(result)
    #             print("start version on device: ", version)
    #             version_in_server = file_name[0].split("_")[-1]
    #             version_in_server = Path(version_in_server).stem
    #             if version_in_server == version:
    #                 if log:
    #                     print("install_start_apk  --4.2")
    #                     """""
    #                     in case function executed from test -> reinstall
    #
    #                     """""
    #                     self.adb_with_wait("install -r  {}", apk_path)
    #                 return True
    #             else:
    #                 print("install_start_apk  --4.3")
    #                 flag = True
    #                 log_html.h_log("{} APK version will be installed".format(version_in_server),style="title_test")
    #                 log_html.h_log("")
    #                 self.adb_with_wait("install -r -d {}", apk_path)
    #                 sleep(5)
    #                 return True
    #     except Exception as e:
    #         print(e)
    #         try:
    #             print(print("install_start_apk  --5"))
    #             if flag:
    #                 print(print("install_start_apk  --5.1"))
    #                 self.restart_adb_server()
    #                 self.adb("install -r -d {}", apk_path)
    #             elif b"versionName" not in result:
    #                 print(print("install_start_apk  --5.2"))
    #                 self.adb("install {}", apk_path)
    #         except Exception as e:
    #             print(print("install_start_apk  --5.3"))
    #             write_internal_error_to_log()
    #             raise e

    def get_apk_path(self):
        apk_directory = str(self.home_path / "apk/")
        file_name = os.listdir(apk_directory)
        print("apk file name", file_name)
        apk_path = apk_directory + "/" + file_name[0]
        return apk_path, file_name

    def force_apk_install(self):
        apk_path, file_name = self.get_apk_path()
        print(apk_path)
        check_output("adb -s {} install -r -d {}".format(self.device, apk_path).split())

    def uninstall_start_app(self):
        self.adb("shell pm uninstall -k com.celltick.lockscreen")

    def clear_cach(self):
        res = False
        try:
            self.adb("shell pm clear com.celltick.lockscreen")
            sleep(2)
            res = settings.test.verify_in_device_home_screen_by_elements()#.verify_in_start_home_screen_by_elements()
        except Exception as e:
            print(e)
        return res

    def uninstall_and_install_for_displayed_permission(self,logging=False,reset_ads_id=False):
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        try:
            self.uninstall_or_clear_cach()
            self.install_start_anyway()
            os.environ["RAV_TUTORIAL_AFTER_FIRST_INSTALLATION"] = "yes"
        except Exception as e:
            print(e)

    def uninstall_or_clear_cach(self):
        print("in uninstall_and_install_for_displayed_permission")
        uninstall_message = self.adb("shell pm uninstall com.celltick.lockscreen")
        if "failure" in str(uninstall_message).lower():
            in_device_home_screen = self.clear_cach()
            if not in_device_home_screen:
                sleep(20)
                self.clear_cach()
        #     return "cach cleared"
        # else:
        print("after uninstall")
        sleep(10)
        device_obj.press.back()

    def get_device_sdk_version(self):
        self.adb("shell getprop ro.build.version.sdk")

    def check_if_phone_has_sim(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        result = None
        try:
            result = self.adb("shell getprop gsm.sim.state")
        except CalledProcessError:
            print("error with sim command")
        if b"READY" in result:
            print("There is sim in device")
            return True
        else:
            print("There is no sim in device")
            return False

    def check_if_device_in_full_screen(self):
        result = self.adb("shell dumpsys window | grep mTopIsFullscreen=")

        is_full_screen = "true" in str(result)
        return is_full_screen

    def get_recent_activity(self):
        adb_command = "adb -s {} shell dumpsys activity | grep {}".format(device_id,'"Recent #0"').split()
        #adb_command.append('Recent #0')
        activity = check_output(adb_command).splitlines()
        return activity

    def multiple_screen_on_and_off(self, on_of_num):
        self.adb("shell input keyevent KEYCODE_POWER")

    def get_physical_device_screen_size(self):
        print("in get_physical_device_screen_size")
        print("self.device", self.device)
        try:
            size = self.adb("shell wm size")
            width_height_size = str(size[0]).split(" ")[2][:-1]
            separate_size = width_height_size.split("x")
            width = separate_size[0]
            height = separate_size[1]
        except Exception as e:
            print(e)
            width, height = self.get_display_screen_size()
        print("device width {} , hieght {}".format(width,height))
        return int(width), int(height)

    def get_display_screen_size(self):
        device_info = self.device_obj.info
        width = device_info["displayWidth"]
        height = device_info["displayHeight"]
        return int(width), int(height)

    def get_device_dpi(self):
        dpi_size = self.adb("shell getprop ro.sf.lcd_density")
        return int(dpi_size)


    def screenshot_by_adb_simple(self, image_name=None) \
            -> str:
        print("in adb screenshot_by_adb_simple")
        wait_time = 5
        image_name  = "Screenshot.png" if not image_name else image_name
        image_name = str(Path(os.environ.get("RAV_DEVICE_FOLDER")) / image_name)
        sleep(wait_time)
        print("image_name:", type(image_name), image_name)
        self.adb("shell screencap -p /sdcard/Screenshot.png")
        try:
            if Path(image_name).exists():
                print("image with this name exists - deleting ")
                check_output("rm {}".format(str(image_name)).split())
                print("deleted")
        except Exception as e:
            print(e)
        self.adb("pull /sdcard/Screenshot.png {}".format(str(image_name)))
        print("image copied from device")
        sleep(1)
        return str(image_name)

    def screenshot_by_adb(self, image_name=None, place: str=None, wait: int=None,name_no_device_folder=None) \
            -> str:
        print("in adb screenshot_by_adb")
        wait_time = wait or (2.5 if place is None else 5)
        name  = "Screenshot.png" if not name_no_device_folder else name_no_device_folder
        image_name = image_name or str(self.device_folder / name)
        sleep(wait_time)
        print("image_name:", type(image_name), image_name)
        self.adb("shell screencap -p /sdcard/Screenshot.png")
        try:
            if Path(image_name).exists():
                print("image with this name exists - deleting ")
                check_output("rm {}".format(str(image_name)).split())
                print("deleted")
        except Exception as e:
            print(e)
        self.adb("pull /sdcard/Screenshot.png {}".format(str(image_name)))
        print("image copied from device")
        sleep(1)
        return str(image_name)

    def screenshot_by_adb_for_chrome(self, image_name) -> None:
        if not image_name:
            image_name = self.device_folder / "Screenshot.png"
        sleep(10)
        self.adb("shell screencap -p /sdcard/Screenshot.png")
        self.adb("pull /sdcard/Screenshot.png {}", image_name)
        sleep(1)

    def take_snapshot_with_sleep(self):
        image_name = self.device_folder / "Screenshot.png"
        print("taking screenshot as: {}".format(image_name))
        sleep(1.5)
        self.screenshot_by_adb(image_name)
        sleep(0.5)
        return image_name

    def get_apk(self):
        print("in get_apk")
        try:
            result = self.adb("shell dumpsys package com.celltick.lockscreen | grep versionName")
            #version = self.parse_adb_result(result)
            result = result.decode("utf-8").strip()
            version = result.split("=")[1]
            try:
                version = version.split(" ")[0]
            except Exception as e:
                print(e)
            return str(version)
        except Exception as e:
            print("got an exception:")
            print(e)
            return None

    def parse_adb_result(self, result):
        try:
            result = result.decode("utf-8").strip()
        except Exception as e:
            print(e)
        return result
        # result = str(result)
        # result = result.split("\\")
        # version = result[0].split("=")
        # version = version[-1]
        # tmp = version.split("'")
        # if len(tmp)==2:
        #     version = tmp[1]
        # return version

    @staticmethod
    def restart_adb_server():
        check_output("adb kill-server".split())
        sleep(1)
        check_output("adb start-server".split())
        sleep(1)

    def get_device_android_version(self):
        print("in get_device_android_version")
        try:
            result =  self.adb("shell getprop ro.build.version.release")
            return self.parse_adb_result(result)
        except Exception as e:
            print(e)
        return None

    def get_device_api_level(self):
        try:
            result = self.adb("shell getprop ro.build.version.sdk")
            return self.parse_adb_result(result)
        except Exception as e:
            print(e)

    def close_open_apps(self):
        self.adb("shell input keyevent KEYCODE_APP_SWITCH")
        sleep(1.5)
        self.adb("shell input keyevent 20")
        sleep(1.5)
        self.adb("shell input keyevent KEYCODE_DPAD_RIGHT")
        sleep(1.5)
        self.adb("shell input keyevent KEYCODE_DPAD_RIGHT")
        # check_output("adb shell input keyevent KEYCODE_DPAD_TAB".split())
        sleep(1.5)
        self.adb("shell input keyevent KEYCODE_ENTER")

    def go_to_device_home_screen(self):
        cmd = "shell am start -a android.intent.action.MAIN -c android.intent.category.HOME"
        self.adb(cmd)
        sleep(2)

    def go_to_Start_main_page(self):
        cmd = "shell input keyevent KEYCODE_HOME"
        self.adb(cmd)
        sleep(2)

    def verify_change_language_installed(self):
        print("in verify_change_language_installed")
        cmd = "shell dumpsys package | grep AdbChangeLanguage"
        return self.adb(cmd)

    def install_change_language(self):
        print("in install_change_language")
        try:
            apk_path = "/home/share/workspace/apk_change_language/ADB_Change_Language_v0.80_apkpure.com.apk"
            self.adb_with_wait("install -r -d {}", apk_path)
            return True
        except Exception as e:
            print(e)
            return False

    def change_language(self,deviceId, language_code):
        print("in change_language")
        self.grant_permission_to_change_languages()
        sleep(4)
        cmd2 = "adb -s {} shell am start -n net.sanapeli.adbchangelanguage/.AdbChangeLanguage -e language {}".format(
            deviceId, language_code)
        print("cmd ", cmd2)
        sleep(1)
        a = check_output(cmd2.split())
        print("res ", a)

    def grant_permission_to_change_languages(self):
        print("in grant_permission_to_change_languages")
        device_id = self.device
        cmd1 = "adb -s {} shell pm grant net.sanapeli.adbchangelanguage android.permission.CHANGE_CONFIGURATION".format(
            device_id)
        print("adb grant permission ", cmd1)
        res = check_output(cmd1.split())
        print("res ", res)
        return True