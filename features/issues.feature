# Created by itzik at 3/15/18
Feature: Issues
  Provoke an issue by exiting "Widget Color"
  screen by pressing home button then swipe left and click on "Settings"

  Scenario: clicking on settings button and getting to wrong screen
    Given open start
    Then stop recording and save by name "before_clicking_on_settings_and_getting_to_wrong_screen"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on "Setup Your Widgets" in settings screen
    And clicking on widget "Widget Color"
    And press on home button
    Then verify device home screen appears
    Given open start
    When swipe left
    And clicking on settings
    And go to widgets screen
    And click on Widget Alignment
    And wait for "2" seconds
    And press on home button
    Given open start
    And wait for "2" seconds
    And swipe left and click on settings
    And go to widgets screen
    And click on Widget Size
    And press on home button
    Given open start
    And swipe left and click on settings
    And scroll and click on item named: "Setup Your Wallpaper"
    And press on home button
    Given open start
    And swipe left and click on settings
    And scroll and click on item named: "Screen Sleep Timer"
    And press on home button
    Given open start
    And swipe left and click on settings
    And scroll and click on item named: "Notification Selection"
    And press on home button
    Given open start
    And swipe left and click on settings





