# Created by itzik at 4/17/18
Feature: Widgets
  # Enter feature description here
  Scenario: change clock size
    Given open start
    Then stop recording and save by name "before_change_clock_size"
    And wait for "5" seconds
    Then start video recording
    Then verify Start home page
    And enable clock widget if disabled
    When clicking on back button "3" times
    And wait for "1" seconds
    And swipe left
    And clicking on settings
    And clicking on "Setup Your Widgets" in settings screen
    And clicking on widget "Widget Size"
    Then verify in Widget Size screen
    And minimize clock widget by "70" percents and verify


  Scenario: setup color widget
    Given open start
    Then stop recording and save by name "before_setup_color_widget"
    And wait for "5" seconds
    Then start video recording
    Then verify Start home page
    And enable clock widget if disabled
    When clicking on back button "3" times
    And wait for "1" seconds
    And swipe left
    And clicking on settings
    And clicking on "Setup Your Widgets" in settings screen
    And clicking on widget "Widget Color"
    Then set widgets color to black and verify change


  Scenario: enable clock widget
    Given open start
    Then stop recording and save by name "before_enable_clock_widget"
    And wait for "5" seconds
    Then start video recording
    And disable clock widget if enabled
    When clicking on back button "2" times
    Then verify Start home page
    And swipe left without verification
    And wait for "2" seconds
    And clicking on settings
    And clicking on "Setup Your Widgets" in settings screen
    And enable widget - "Clock Widget"
    When clicking on back button "2" times
    Then verify that the clock widget displayed


  Scenario: drag widget
    Given open start
    And disable clock widget if enabled
    When clicking on back button "2" times
    Then verify Start home page
    And swipe left without verification
    And wait for "2" seconds
    And clicking on settings
    And clicking on "Setup Your Widgets" in settings screen
    And clicking on widget "Widget Size"
    Then verify in Widget Size screen
    And maximize date widget
    When clicking on back button "4" times
    Then stop recording and save by name "before_drag widget"
    And wait for "5" seconds
    Then start video recording
    Then drag widget and verify


  Scenario: drag widget to trash
    Given open start
    And disable clock widget if enabled
    When clicking on back button "2" times
    Then verify Start home page
    And swipe left without verification
    And wait for "2" seconds
    And clicking on settings
    And clicking on "Setup Your Widgets" in settings screen
    And clicking on widget "Widget Size"
    Then verify in Widget Size screen
    And maximize date widget
    When clicking on back button "4" times
    Then stop recording and save by name "before_drag widget"
    And wait for "5" seconds
    Then start video recording
    Then drag widget to trash and verify












