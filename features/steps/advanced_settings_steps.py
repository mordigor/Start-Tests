from behave import *
#from hamcrest import assert_that, equal_to
import sys
sys.path.append('../')
import  start_interface as interface
import start_tests_header as s_header

use_step_matcher("re")
use_step_matcher("cfparse")

#
# @when("swipe left")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     assert interface.swipe_left_by_menu_icon(behave=True), s_header.take_screenshot_on_error("failed to swipe left")#print("FAileddddd")#interface.test.swipe_menu()#
#     print("000000")
#
#
# @given("open start")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     print("llllooooooooooooooooooooooool")
#     assert interface.test.open_start_simple(behave=True),s_header.take_screenshot_on_error("failed to open Start")

#
# @step("clicking on settings")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     assert interface.test.click_on_setting_button(behave=True), s_header.take_screenshot_on_error("not in Settings screen")
#     print("11111111111")


@step("clicking on advanced settings")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.test.click_on_advanced_settings(behave=True), s_header.take_screenshot_on_error("not in Advanced Settings screen")
    print("2222222222222")


@then("verify hints not appear when pressing on hubs")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("in verify hints not appear when pressing on hubs")
    assert interface.test.verify_hints_absent(), s_header.take_screenshot_on_error("hints still appear")

#
# @step("The hints of the hubs not appear")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     pass


@step('disable "{thing}" in advanced settings')
def step_impl(context, thing):
    """
    :type context: behave.runner.Context
    """
    print("thing is: ",thing)
    assert interface.test.enable_disable_advanced_settings(text=thing, to_enable=False, behave=True), s_header.take_screenshot_on_error('failed to disable {}'.format(thing))


@step('enable "{setting}" in advanced settings')
def step_impl(context, setting):
    """
    :type context: behave.runner.Context
    """
    print("thing is: ", setting)
    assert interface.test.enable_disable_advanced_settings(text=setting, to_enable=True,
                                                           behave=True), s_header.take_screenshot_on_error(
        'failed to disable {}'.format(setting))


@step('verify full screen in start home screen')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("in verify full screen in start home screen")
    assert interface.test.verify_full_screen(behave=True), s_header.take_screenshot_on_error(" "": to enable Full "
                                                               "Screen")


@step('verify that menu is abscent in start home screen')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("in verify that menu is abscent in start home screen")
    assert interface.test.verify_menu_is_absent_on_main_screen(behave=True), s_header.take_screenshot_on_error(" "": "
                                                                                 "to disable menu")

#
# @step('verify in Start home screen')
# def step_impl(context):
#     print("in verify in Start home screen")
#     assert interface.back_from_settings_screen(behave=True), s_header.take_screenshot_on_error("not in Srart home page")

