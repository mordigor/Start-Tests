import time
from behave import *
import sys
sys.path.append('../')
import start_interface as interface
import start_tests_header as s_header
use_step_matcher("re")
use_step_matcher("cfparse")


@given("it is possible to simulate manual install on device")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("it is possible to simulate manual install on device")
    interface.verify_downloads_manager_installed()
