import time
import signal
import os
from behave import *
import sys
sys.path.append('../')
import start_interface as interface
import start_tests_header as s_header
use_step_matcher("re")
use_step_matcher("cfparse")


@given("install or reinstall start on device")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.test.adb.install_start_apk(is_test=True), s_header.take_screenshot_on_error(
        "install Start failure")


@step("click on back button")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.press_back(logging=True,behave=True)
    time.sleep(1)


@when("swipe left")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.swipe_left_by_menu_icon(behave=True), s_header.take_screenshot_on_error("failed to swipe left")#print("FAileddddd")#interface.test.swipe_menu()#
    print("after swiping left")


@step("swipe left without verification")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.swipe_left_without_verification()


@step("swipe left")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.swipe_left_by_menu_icon(behave=True), s_header.take_screenshot_on_error("failed to swipe left")#print("FAileddddd")#interface.test.swipe_menu()#
    print("after swiping left")


@step("swipe left and click on settings")
def step_impl(context):
    assert interface.swipe_and_click_on_settings()


@given("open start")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("llllooooooooooooooooooooooool")
    assert interface.test.open_start_simple(behave=True),s_header.take_screenshot_on_error("failed to open Start")


@step("open start")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.open_start()


@step("open start on the first time")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.open_start_first_time(logging=True)


@step("clicking on settings")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.test.click_on_setting_button(behave=True), s_header.take_screenshot_on_error("not in Settings screen")
    print("11111111111")


@step("verify Start home page")
def step_impl(context):
    """
        :type context: behave.runner.Context
        """
    assert s_header.test.verify_in_start_home_screen_by_elements(logging=True,behave=True), s_header.take_screenshot_on_error("not in Start home screen")


@step('clicking on "{setting}" in settings screen')
def step_impl(context,setting):
    """
    :type context: behave.runner.Context
    """
    interface.test.scroll_and_click_by_text(setting,logging=True)


@then ("verify contact us screen contains 3 options")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.verify_in_contact_us_screen(), s_header.take_screenshot_on_error("not in contact us screen")


@then ("verify tutorial opened and has all the screens")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.verify_tutorial_opened(), s_header.take_screenshot_on_error("not in tutorial or missing tutorial screens")


@then('verify device home screen appears')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.test.verify_in_device_home_screen_by_elements(logging=True), s_header.take_screenshot_on_error("not in device home screen")


@when('exit start by swiping to unlock hub')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.test.slide_to_unlock_hub_reached_to_device_home_screen(), s_header.take_screenshot_on_error("not in device home screen")


@step('set screen state to "{state}"')
def step_impl(context,state):
    """
    :type context: behave.runner.Context
    """
    if state.lower() == "off":
        assert interface.set_screen_state_off(), s_header.take_screenshot_on_error("screen OFF failed")
    else:
        assert interface.set_screen_state_on(), s_header.take_screenshot_on_error("screen ON failed")


@step('press on home button')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("in press on home button")
    interface.press_on_home_button()
    time.sleep(2)


@given('android version higher or equal to 6.0')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    #todo: change
    assert interface.verify_android_version_higher_or_equel_to_6(), s_header.take_screenshot_on_error("android version is lower the 6.0 can not proceed with current test")


@when('install start on the first time or clear cach if preloaded')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.adb_functions.uninstall_and_install_for_displayed_permission(logging=True)


@step('skiping tutorial if needed')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.skip_tutorial_if_needed()


@then('skip tutorial')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.skip_tutorial_by_ok(), s_header.take_screenshot_on_error("skiping tutorial failed")


@then('allow all permissions')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.allow_permissions_on_start_first_time_install(), s_header.take_screenshot_on_error("misssing permissions screens or not in the right order -> contacts,video, phone,SMS,location,photos")


@then('dismiss al permission requests')
def step_impl(context):
    assert interface.dismiss_permission_requests(), s_header.take_screenshot_on_error("wrong number of permissions or else")


@step('skip keep in touch screen')
def step_impl(context):
    assert interface.skip_keep_in_touch_screen_if_needed(), s_header.take_screenshot_on_error("did not get keep in touch screen")


@step('skip welcome if needed')
def step_impl(context):
    interface.skip_welcome_if_needed()


@step('skip questionary if needed')
def step_impl(context):
    interface.skip_questionary_if_needed()


@step('dismiss allow to share installed appplications list')
def step_impl(context):
    interface.dismiss_allow_to_share_installed_applications(logging=True)


@then('start video recording')
def step_impl(context):
    p_id = os.environ.get("RAV_CURRENT_VIDEO_PROCESS_ID")
    if p_id:
        p_id = int(p_id)
        try:
            os.kill(p_id, signal.SIGTERM)
        except Exception as e:
            print(e)
    s_header.start_recording()
    #os.environ["RAV_CURRENT_VIDEO_PROCESS_ID"] = str(new_p_id)


@step('fail test')
def step_impl(context):
    assert interface.fail_test_to_see_the_video(),s_header.take_screenshot_on_error("just for video")


@step('open start without verification')
def step_impl(context):
    interface.open_start_without_verification()


@step('wait for "{sec}" seconds')
def step_impl(context,sec):
    time.sleep(int(sec))


@step('swipe to multimedia hub')
def step_impl(context):
    assert interface.open_camera_by_swiping_to_multimedia_hub(),s_header.take_screenshot_on_error("swipe failed")


@then('verify that does not asks for access permissions')
def step_impl(context):
    assert interface.verify_no_request_for_access_permission(), s_header.take_screenshot_on_error("asks for access permission")


@then('verify that camera was opened')
def step_impl(context):
    assert interface.verify_camera_opened(), s_header.take_screenshot_on_error("camera was not opened")

# @step('taking screenshot named "{image_name}"')
# def step_impl(context,image_name):
#     pass


@step('wait for welcome button after installation up to "{seconds}" seconds')
def step_impl(context,seconds):
    interface.wait_for_welcome_button_after_install(seconds)


@then('verify request for access permission')
def step_impl(contetxt):
    assert interface.verify_request_for_access_permission(), s_header.take_screenshot_on_error("no request for access permission")


@then('stop recording and save by name "{file_name}"')
def step_impl(contetxt,file_name):
    interface.stop_recording_and_save_by_name(file_name)


@then('unlock device')
def step_impl(context):
    interface.unlock_device()


@when('test')
def step_impl(context):
    s_header.interface.set_widget_color_to_default()
    s_header.interface.reset_widget_size()


@then('verify no permission request dispalyed for contacts and physical location')
def step_impl(context):
    assert interface.verify_no_permission_request_displayed_for_contacts_and_physical_location(), \
        s_header.take_screenshot_on_error("request for permission when already allowed")


@then('verify no permission request for file access')
def step_impl(context):
    assert interface.verify_no_permission_request_for_file_access(),\
        s_header.take_screenshot_on_error("request for permission when already alloweds")


@step('go to widgets screen')
def step_impl(context):
    interface.go_to_setup_your_widget_screen(logging=True)


@step('click on Widget Alignment')
def step_impl(context):
    interface.click_on_widget_alignment()


@step('click on Widget Size')
def step_impl(context):
    interface.click_on_Widget_Size()


@step('scroll and click on item named: "{item}"')
def step_impl(context,item):
    interface.test.scroll_and_click_by_text(item)


@step('take screenshot before change')
def step_impl(context):
    interface.test.adb.screenshot_by_adb_simple("Screenshot.png")