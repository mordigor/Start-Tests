# Created by user at 2/22/18

Feature: Advanced Settings

  Scenario: disable show hints
    Given open start
    Then stop recording and save by name "before_disable_show_hints"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on advanced settings
    And disable "Show Hints" in advanced settings
    And click on back button
    And click on back button
    Then verify hints not appear when pressing on hubs


  Scenario: enable full screen
    Given open start
    Then stop recording and save by name "before_enable_full_screen"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on advanced settings
    And enable "Full Screen" in advanced settings
    And click on back button
    And click on back button
    Then verify full screen in start home screen


  Scenario: disable show settings icon
    Given open start
    Then stop recording and save by name "before_disable_show_settings_icon"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on advanced settings
    And disable "Show Settings Icon" in advanced settings
    And click on back button
    And click on back button
    And take screenshot before change
    And wait for "5" seconds
    Then verify that menu is abscent in start home screen





