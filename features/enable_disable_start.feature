# Created by itzik at 3/15/18
Feature: Enable Disable
  # Enter feature description here

   Scenario: disable and enable start
    Given open start
    Then stop recording and save by name "before_disable_start"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on "Disable Start" in settings screen
    Then verify disable start appears
    And click on disable start choice button
    Then verify that disable periods appear
    And clicking on ok button in disable start periods screen
    Then verify device home screen appears
    And set screen state to "Off"
    And wait for "1" seconds
    And set screen state to "On"
    And wait for "1" seconds
    And unlock device
    And wait for "2" seconds
    Then verify device home screen appears
    Given open start
    Then stop recording and save by name "before_enable_start"
    And wait for "5" seconds
    Then start video recording
    When exit start by swiping to unlock hub
    And set screen state to "Off"
    And wait for "1" seconds
    And set screen state to "On"
    And dismiss allow to share installed appplications list
    And wait for "2" seconds
    Then verify Start home page








