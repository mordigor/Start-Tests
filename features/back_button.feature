# Created by itzik at 3/12/18

Feature: back button

 Scenario: back from settings
    Given open start
    Then stop recording and save by name "before_back_from_settings"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And click on back button
    Then verify Start home page

 Scenario: back from security
    Given open start
    Then stop recording and save by name "before_back_from_security"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on security
    And click on back button
    Then verify Start home page

 Scenario: back from first drawer
    Given open start
    Then stop recording and save by name "before_back_from_drawer"
    And wait for "5" seconds
    Then start video recording
    When open first drawer
    And wait for "5" seconds
    And click on back button
    And wait for "3" seconds
    Then verify Start home page

 Scenario: back from second drawer
    Given open start
    Then stop recording and save by name "before_back_from_drawer"
    And wait for "5" seconds
    Then start video recording
    When open second drawer
    And wait for "5" seconds
    And click on back button
    And wait for "3" seconds
    Then verify Start home page


#Scenario: test
#    When test


