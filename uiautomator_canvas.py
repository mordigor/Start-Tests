import os
import sys
from pathlib import Path, PurePath
from typing import Union
from time import sleep
from PIL import Image
import cv2
from cv2.cv2 import imread
import numpy as np
import pyocr.builders
import pytesseract
from uiautomator import JsonRPCError

import settings
import config

device_id = settings.device_id
device_obj = settings.device_obj
adb_functions = settings.adb_functions

PathOrStr = Union[Path, PurePath, str]
TOOL = None
# noinspection PyUnresolvedReferences
cv2_imread = cv2.imread  # Overcome a cv2 hidden declaration


def init_pyocr():
    global TOOL
    tools = pyocr.get_available_tools()
    if len(tools) == 0:
        print("No OCR tool found")
        sys.exit(1)
    TOOL = tools[0]


def verify_lock_in_the_middle(x, w, screen_width):
    if w == screen_width:
        return False
    middle_cv2 = int(x + w / 2)
    middle_screen = int(screen_width / 2)
    allowed_inaccuracy = int(screen_width / 100)
    if middle_screen - allowed_inaccuracy < middle_cv2 < middle_screen + allowed_inaccuracy:
        return True
    return False


# noinspection PyPep8Naming
def find_possible_matches(small_image, large_image):
    # Code taken from Stack-Overflow
    # https://stackoverflow.com/questions/29663764/determine-if-an-image-exists-within-a-larger-image-and-if-so-find-it-using-py

    # This code finds possible matches.
    small_image = cv2_imread(small_image)
    large_image = cv2_imread(large_image)
    search_img_dest = np.atleast_3d(large_image)
    img_to_look_for = np.atleast_3d(small_image)
    H, W, D = search_img_dest.shape[:3]
    height, width = img_to_look_for.shape[:2]
    # Integral image and template sum per channel
    sat = search_img_dest.cumsum(1).cumsum(0)
    tpl_sum = np.array([img_to_look_for[:, :, i].sum() for i in range(D)])
    # Calculate lookup table for all the possible windows
    iA, iB, iC, iD = sat[:-height, :-width], sat[:-height, width:], sat[height:, :-width], sat[
                                                                                           height:,
                                                                                           width:]
    lookup = iD - iB - iC + iA
    # Possible matches
    possible_match = np.where(
        np.logical_and(*[lookup[..., i] == tpl_sum[i] for i in range(D)]))
    return img_to_look_for, possible_match, search_img_dest, height, width


def get_number_of_white_pixels(img=None):
    print("in get_number_of_white_pixels")
    if img is None:
        img = adb_functions.screenshot_by_adb()
    print("55")
    print("type of image: ",type(img))
    if isinstance(img, str):
        img = cv2.imread(img)
    WHITE_MIN = np.array([235, 235, 235], np.uint8)
    WHITE_MAX = np.array([255, 255, 255], np.uint8)
    dst = cv2.inRange(img, WHITE_MIN, WHITE_MAX)
    num_white = cv2.countNonZero(dst)
    print('The number of white pixels is: ' + str(num_white))
    return num_white


def get_number_of_black_pixels(img=None):
    if not img:
        img = adb_functions.screenshot_by_adb()
    if isinstance(img, str):
        img = cv2.imread(img)
    BLACK_MIN = np.array([0, 0, 0], np.uint8)
    BLACK_MAX = np.array([30, 30, 30], np.uint8)
    dst = cv2.inRange(img, BLACK_MIN, BLACK_MAX)
    num_black = cv2.countNonZero(dst)
    print('The number of black pixels is: ' + str(num_black))
    return num_black


def is_black_background(img):
    if isinstance(img, str):
        img = cv2.imread(img)
    height, width, channels = img.shape
    print("height: ", height, ", width: ", width)
    num_of_black = get_number_of_black_pixels(img)
    if height == 0 or width == 0:
        return None
    ratio = num_of_black / (height * width)
    print("the ratio is: ", ratio)
    if ratio > 0.6:
        return True
    return False


def get_bounds(start_index=None, end_index=None):
    if not start_index:
        start_index = 0
    if not end_index:
        end_index = 60
    dict_ = {}
    # print("end_index ", end_index)
    for index, item in enumerate(device_obj(
            className="android.view.View")):
        # print("current index ",index)
        # print("item: ",item)
        # print("info ",item.info)
        if start_index <= index <= end_index:
            print("index ", index, " in loop")
            selected = item.selected
            bounds = item.bounds
            print("bonds in get_bounds: ", bounds)
            # if not verify_item_in_screen(bounds):
            # #     return dict_ , 0
            # print("now", item.selected)
            # print("boundaries ", item.bounds)
            dict_["{}".format(index)] = [selected, bounds, item]
        if index > end_index:
            # print("breaking")
            break
            # item.scroll()
            # print("DICT: ", dict_)
    return dict_, 1


def is_black_theme(item):
    path = Path(os.environ.get("RAV_DEVICE_FOLDER")) / config.abstract_page_img_name
    new = Path(os.environ.get("RAV_DEVICE_FOLDER")) / config.abstract_page_img_name_cropped
    img = cv2.imread(str(path))

    x_left = item['left']
    x_right = item['right']
    y_up = item['top']
    y_bottom = item['bottom']
    delta_y = int((y_bottom - y_up) / 10)
    delta_x = int((x_right - x_left) / 10)
    y_up = y_up + delta_y
    y_bottom = y_bottom - delta_y
    x_left = x_left + delta_x
    x_right = x_right - delta_x
    print("x_left ", x_left, "x_right ", x_right)
    crop_img = img[y_up:y_bottom, x_left:x_right]
    cv2.imwrite(str(new), crop_img)
    return is_black_background(crop_img)


def turn_all_non_black_to_white(image_path,save_path):
    im = cv2.imread(image_path)
    im[np.where((im > [10, 0, 0]).all(axis=2))] = [255, 255, 255]
    cv2.imwrite(save_path, im)


class CanvasAutomation:
    def __init__(self):
        self.device_obj = device_obj
        self.device_id = device_id
        self.home_path = Path(os.environ['HOME_DIRECTORY'])
        self.device_folder =  str(Path(os.environ.get('RAV_DEVICE_FOLDER')))
        self.adb = adb_functions
        init_pyocr()

    @staticmethod
    def calculate_center_image(small_image: PathOrStr):

        if not os.path.exists(str(small_image)):
            print("ATTENTION !! FILE NOTE EXIST -> {} ".format(small_image))
            return None, None

        small_image = Image.open(str(small_image))
        x, y = small_image.size
        return x / 2, y / 2

    @staticmethod
    def to_gray_scale(arr):
        if len(arr.shape) == 3:
            return np.average(arr, -1)  # average over the last axis (color channels)
        else:
            return arr

    @staticmethod
    def normalize(arr):
        rng = arr.max() - arr.min()
        min_val = arr.min()
        return (arr - min_val) * 255 / rng

    @staticmethod
    def compare_images(current_screenshot, saved_pic, allowed_dif=0.6):
        try:
            print("DEBUG compare images")
            img1 = CanvasAutomation.to_gray_scale(imread(str(current_screenshot)).astype(float))
            img2 = CanvasAutomation.to_gray_scale(imread(str(saved_pic)).astype(float))
            # normalize to compensate for exposure difference, this may be unnecessary
            # consider disabling it
            img1 = CanvasAutomation.normalize(img1)
            img2 = CanvasAutomation.normalize(img2)
            # calculate the difference and its norms
            diff = img1 - img2
            m_norm = sum(abs(diff))  # Manhattan norm
            # noinspection PyUnresolvedReferences
            df = m_norm / img1.size
            print("DEBUG the images different percent:", df.sum())
            return "1" if df.sum() < allowed_dif else "0"
        except Exception as e:
            print("DEBUG: in compare_images - get an exception")
            print(e)
            return None

    def crop_images(self, image, screen_width, screen_height, name):
        img = cv2.imread(str(image))
        crop_img = img[int(0.1 * screen_height):screen_height, 0:screen_width]
        # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
        new_image = str(self.device_folder / "cropped_{}.png".format(name))
        cv2.imwrite(new_image, crop_img)
        return new_image

    def get_img_coordinate(self, small_image: PathOrStr, large_image: PathOrStr):
        print("in get_img_coordinate")
        if not os.path.exists(str(small_image)):
            print("ATTENTION !! FILE NOTE EXIST -> {} ".format(small_image))
            return None, None
        if not large_image:
            print("take large screenshot")
            large_image = self.adb.take_snapshot_with_sleep()
        try:
            add_x, add_y = self.calculate_center_image(small_image)
            small_image = cv2.imread(str(small_image))
            large_image = cv2.imread(str(large_image))
            result = cv2.matchTemplate(small_image, large_image, cv2.TM_CCOEFF_NORMED)
            # We want the minimum squared difference
            y, x = np.unravel_index(result.argmax(), result.shape)
            x = x.tolist()
            y = y.tolist()
            print(x, y)
            print("add_x, add_y:", add_x, add_y)
            point = (int(x + add_x), int(y + add_y))
            print("point ", point)
            return point
        except Exception as e:
            print(e)
            return None

    @staticmethod
    def exact_img_coordinate(img_to_look_for, possible_match, search_img_dest, height,
                             width) -> tuple:
        """

        :param img_to_look_for:
        :param possible_match:
        :param search_img_dest:
        :param height:
        :param width:
        :return:
        """
        # Find exact match from possible matches
        for y, x in zip(*possible_match):
            # noinspection PyTypeChecker
            if np.all(search_img_dest[y + 1:y + height + 1,
                      x + 1:x + width + 1] == img_to_look_for):
                x = x.tolist()
                y = y.tolist()
                point = (x, y)
                return point

    def enter_app_name_for_shortcut(self, app_name: str, screen_width, screen_height) -> bool:
        print("inside enter app name func")
        try:
            edit_field_id = "com.celltick.lockscreen:id/app_name_edit"
            edit_resource = self.device_obj(resourceId=edit_field_id)
            print("first try to enter app name")
            sleep(2)
            self.device_obj.click(int(screen_width / 2), int(screen_height * 0.07))
            sleep(1)
            edit_resource.set_text(app_name)
            print("finish enter app name")
            return True
        except JsonRPCError:
            try:
                adb_functions.restart_adb_server()
                edit_resource = self.device_obj(resourceId=edit_field_id)
                self.device_obj.click(int(screen_width / 2), int(screen_height * 0.07))
                sleep(1)
                edit_resource.set_text(app_name)
                return True
            except Exception as e:
                try:
                    print("second try to enter app name")
                    edit_resource = self.device_obj(resourceId=edit_field_id)
                    edit_resource.clear_text()
                    sleep(1)
                    edit_resource.set_text(app_name)
                    sleep(2)
                    return True
                except Exception as e:
                    print("Failed to add shortcut", e)

            return False

    def verify_action(self, small_image: PathOrStr, large_image: PathOrStr):
        print("in verify_action")
        print("small_image ", small_image)
        print("large_image ", large_image)
        try:
            if not large_image:
                large_image = self.adb.take_snapshot_with_sleep()
            small_image_ = cv2.imread(str(small_image))
            large_image_ = cv2.imread(str(large_image))
            method = cv2.TM_CCOEFF_NORMED
            # w, h = large_image_.shape[:-1]
            res = cv2.matchTemplate(small_image_, large_image_, method)
            threshold = 0.99
            threshold_low = 0.7
            step = 0.02
            while threshold >= threshold_low:
                loc = np.where(res >= threshold)
                if not loc[0].any():
                    threshold = round((threshold - step), 2)
                    continue
                pt = None
                for pt in zip(*loc[::-1]):
                    print(pt)
                    # cv2.rectangle(large_image_, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
                    # cv2.imwrite("result.png", large_image_)
                print("threshold for img {} : {}".format(small_image, threshold))
                x = pt[0].tolist()
                y = pt[1].tolist()
                point = (x, y)
                print("point ", point)
                return point
        except Exception as e:
            print(e)
            return None

    def verify_exact_match(self, small_image: PathOrStr, large_image: PathOrStr):
        try:
            if not large_image:
                large_image = self.adb.take_snapshot_with_sleep()
            img_to_look_for, possible_match, search_img_dest, height, width = find_possible_matches(
                str(small_image), str(large_image))
            point = self.exact_img_coordinate(img_to_look_for, possible_match, search_img_dest,
                                              height, width)

            filename = Path(small_image).stem
            print(filename, "appears" if point is not None else "does not appear", "in strip")
            return point
        except Exception as e:
            print(e)
            return None

    @staticmethod
    def identify_small_image_occurrences_in_large_image(small_image: PathOrStr,
                                                        large_image: PathOrStr) -> int:
        objects_arr = []
        if not os.path.isfile(str(small_image)):
            return 0
        img_rgb = cv2.imread(str(large_image))
        template = cv2.imread(str(small_image))
        # w, h = template.shape[:-1]
        res = cv2.matchTemplate(img_rgb, template, cv2.TM_CCOEFF_NORMED)
        threshold = .98
        loc = np.where(res >= threshold)
        for pt in zip(*loc[::-1]):  # Switch columns and rows
            # cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
            objects_arr.append(pt)
        # cv2.imwrite('result.png', img_rgb)
        return len(objects_arr)

    def identify_all_app_objects_and_crop(self, hub_path, screen_width, screen_height):
        # contours, image = CanvasAutomation.get_objects_contours(hub_path, (6, 6))
        path = Path(hub_path)
        if path.exists():
            contours, image = CanvasAutomation.get_objects_contours(hub_path, (2, 2))
            object_coordinates_arr = []
            images_details = []
            for c in contours:
                x, y, w, h = cv2.boundingRect(c)
                # noinspection PyChainedComparisons
                if w > screen_width / 50 and y > screen_height / 3:
                    if "LG" not in str(self.device_folder):
                        self.filter_app_object(h, hub_path, image, images_details,
                                               object_coordinates_arr, screen_width, w, x, y)
                    elif y < int(screen_height * 0.925):
                        self.filter_app_object(h, hub_path, image, images_details,
                                               object_coordinates_arr, screen_width, w, x, y)
            print("object_coordinates_arr ", object_coordinates_arr)
            return object_coordinates_arr, images_details
        else:
            print("DEBUG: hun path {} doesn't exists".format(hub_path))

    def filter_app_object(self, h, hub_path, image, images_details, object_coordinates_arr,
                          screen_width, w, x, y):
        if "media" in hub_path:
            if (image[int(y + h * 0.5)][int(x + w * 0.5)] != np.array([0, 0, 0])).any():
                if w < screen_width * 0.18 and x != 0 and y != 0:
                    # maybe take all
                    print("inside", image[int(y + h * 0.5)][int(x + w * 0.5)])
                    print(x, y, w, h)
                    object_coordinates_arr.append((int(x + w / 2), int(y + h / 2)))
                    new_img = image[y:y + h, x:x + w]
                    images_details.append(new_img)
        else:
            if (image[int(y + h * 0.5)][int(x + w * 0.5)] != np.array([0, 0, 0])).any():
                if w < screen_width / 4:
                    object_coordinates_arr.append((int(x + w / 2), int(y + h / 2)))
                    new_img = image[y:y + h, x:x + w]
                    images_details.append(new_img)
                    # if (image[int(y + h * 0.5)][int(x + w * 0.5)] != np.array(
                    #         [179, 179, 179])).any() and "multimedia" not in image:

    @staticmethod
    def get_objects_contours(image, rect):
        print("IMAGE EEE ", image)
        image = cv2.imread(image)
        edged = cv2.Canny(image, 10, 250)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, rect)
        closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
        # finding_contours
        cnts, contours, hierarchy = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL,
                                                     cv2.CHAIN_APPROX_SIMPLE)
        return contours, image

    def identify_all_hubs_and_crop_to_images(self, screen_height, screen_width):
        print("----in identify_all_hubs_and_crop_to_images")
        image = str(self.device_folder / "select_options.png")
        contours, image = CanvasAutomation.get_objects_contours(image, (7, 7))
        object_coordinates_arr = []
        images_details = []
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            if "LG" not in str(self.device_folder):
                if w > screen_width / 50 and y > int(screen_height / 2.5):
                    if w < screen_width / 4:
                        object_coordinates_arr.append((int(x + w / 2), int(y + h / 2)))
                        new_img = image[y:y + h, x:x + w]
                        images_details.append(new_img)
            else:
                if w > screen_width / 50 and int(screen_height / 2.5) < y < int(screen_height
                                                                                        * 0.925):
                    if w < screen_width / 4:
                        object_coordinates_arr.append((int(x + w / 2), int(y + h / 2)))
                        new_img = image[y:y + h, x:x + w]
                        images_details.append(new_img)
        print("num of objects found: ", len(object_coordinates_arr))
        return object_coordinates_arr, images_details

    @staticmethod
    def identify_lock_object(image, screen_height, screen_width):
        if not image:
            image = adb_functions.screenshot_by_adb()
        contours, image = CanvasAutomation.get_objects_contours(image, (2, 2))
        object_coordinates_arr = []
        images_details = []
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            if verify_lock_in_the_middle(x, w, screen_width) and (screen_height * 0.6) < y < (
                        screen_height - screen_height / 8):
                object_coordinates_arr.append((int(x + w / 2), int(y + h / 2)))
                new_img = image[y:y + h, x:x + w]
                images_details.append(new_img)

        print("lock object point", object_coordinates_arr)
        return object_coordinates_arr, images_details

    @staticmethod
    def identify_delete_object(image, screen_height):
        sleep(2)
        contours, image = CanvasAutomation.get_objects_contours(image, (4, 4))
        object_coordinates_arr = []
        images_details = []
        for c in contours:

            x, y, w, h = cv2.boundingRect(c)
            if x != 0 and int(screen_height * 0.23) < y < int(screen_height * 0.7):
                if (image[int(y + h * 0.2)][int(x + w * 0.2)] == np.array([255, 0, 0])).any():
                    sleep(1)
                    object_coordinates_arr.append((x, y))
                    new_img = image[y:y + int(h / 2), x:x + int(w / 2)]
                    images_details.append(new_img)
        print("delete object point", object_coordinates_arr)
        return object_coordinates_arr, images_details

    @staticmethod
    def search_text_in_image(img, search_text):
        print("in search_text_in_image")
        # Recognize text with tesseract for python
        result = pytesseract.image_to_string(Image.open(img))
        print("search_text ",search_text," extracted text ",result)
        if search_text in result:
            print(result)
            return True
        return False
